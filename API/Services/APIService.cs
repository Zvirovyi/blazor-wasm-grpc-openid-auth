using Grpc.Core;
using API;
using ProtoAPI;

namespace API.Services;

public class APIService : ProtoAPI.API.APIBase
{
    private readonly ILogger<APIService> _logger;

    public APIService(ILogger<APIService> logger)
    {
        _logger = logger;
    }

    public override Task<RegisterReplyMessage> Register(RegisterMessage request, ServerCallContext context)
    {
        return base.Register(request, context);
    }

    public override Task<StringMessage> SecureTest(StringMessage request, ServerCallContext context)
    {
        return base.SecureTest(request, context);
    }
}